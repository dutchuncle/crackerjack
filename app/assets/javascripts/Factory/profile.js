/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict'

var serviceModule = angular.module('crackerjack.Managers')

serviceModule.factory('profileFactory',['$http', '$q', function($http, $q){
    
    var profileFactory = {
        
        _currentProfile: null,
        
        getCurrentProfile: function(){
            
            var deferred = $q.defer();
            
            var scope = this;
            
            if(scope._currentProfile){
                deferred.resolve(scope._currentProfile);
            }
            else{
                $http.get('/currentprofile.json')
                        .success(function(data, status, headers, config){
                            scope._currentProfile = data;
                            deferred.resolve(data);
                        })
                        .error(function(){
                            deferred.reject();
                        })
            }
            return deferred.promise;
        },
        
        saveProfile: function(profile){
            
            var deferred = $q.defer();
            
            $http.patch('/profiles/'+profile.id['$oid']+".json", profile)
                    .success(function(data, status, headers, config){
                        deferred.resolve(data);
                    })
                    .error(function(){
                        deferred.reject();
                    })
            return deferred.promise
        }
        
    }
    
    return profileFactory;
        
}]);
