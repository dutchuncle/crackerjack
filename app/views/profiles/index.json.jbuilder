json.array!(@profiles) do |profile|
  json.extract! profile, :id, :username, :firstname, :lastname, :description
  json.url profile_url(profile, format: :json)
end
