/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('crackerjack.Dashboard', ['ui.router','templates'])
    .config(function($stateProvider, $urlRouterProvider) {
              $stateProvider
                  .state('dashboard', {
                    url: "/dashboard",
                    templateUrl: 'Dashboard/_dashboard.html',
                    controller: 'DashboardCtrl',
                    data:{}
                  })
    })
    .controller('DashboardCtrl', ['$scope', 'serviceFactory', function($scope, serviceFactory) {
            
            serviceFactory.getLatestServices().then(function(data){
                
                $scope.services = data;
                
            });
            

    }]);

