/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('crackerjack.Service', ['ui.router','templates'])
    .config(function($stateProvider, $urlRouterProvider) {
              $stateProvider
                  .state('services', {
                    url: "/services",
                    templateUrl: 'Services/_services.html',
                    controller: 'ServicesCtrl',
                    data:{}
                  });
    })
    .controller('ServicesCtrl', ['$scope', 'serviceFactory', '$modal', function($scope, serviceFactory, $modal) {
            
            serviceFactory.getServices().then(function(data){
                
                console.log(data);
                $scope.services = data;
                
            });
            
            $scope.saveService = function(service){
                
                serviceFactory.saveService(service).then(function(data){
                    
                    if(data){
                        $scope.services.push(data);
                    }
                    
                })
            }
            
            $scope.deleteService = function(service){
                
                if(confirm("Delete service "+service.name+"?")){
                    
                    serviceFactory.deleteService(service.id['$oid']).then(function(status){
                        
                        if(status==204){
                            var index = $scope.services.indexOf(service);
                            $scope.services.splice(index, 1);
                        }

                    })
                    
                }
                
            }
            
            //$scope.animationsEnabled = true;
            
            $scope.message = 'hello world';
            
            $scope.open = function (size) {
                
                //$scope.newService = null;

                var modalInstance = $modal.open({
                    animation: true,
                    templateUrl: 'Services/_new-service.html',
                    controller: 'NewServiceModalInstanceCtrl',
                    size: size,
                    resolve: {
                        message: function() {
                            return $scope.message
                        }
                    }
                });

                modalInstance.result.then(function (service) {
                  //$scope.newService = service;
                  $scope.saveService(service);
                  console.log($scope.newService);
                }, function () {
                  
                });
              };

    }])
    .controller('NewServiceModalInstanceCtrl', function ($scope, $modalInstance) {
        
        $scope.service = {
            name: "",
            description: "",
            week: 0,
            month: 0,
            year: 0
        }
        
        $scope.saveService = function () {
          $modalInstance.close($scope.service);
        };

        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
        
    });


