/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict'

var serviceModule = angular.module('crackerjack.Managers')

serviceModule.factory('serviceFactory',['$http', '$q', function($http, $q){
    
    var serviceFactory = {
        
        getServices: function(){
            
            var deferred = $q.defer();
            
            var scope = this;
            
            $http.get('/myservices.json')
                        .success(function(data, status, headers, config){
                            deferred.resolve(data);
                        })
                        .error(function(){
                            deferred.reject();
                        });
            
            return deferred.promise;
            
        },
        
        getAllServices: function(){
            
            var deferred = $q.defer();
            
            var scope = this;
            
            $http.get('/services.json')
                        .success(function(data, status, headers, config){
                            deferred.resolve(data);
                        })
                        .error(function(){
                            deferred.reject();
                        });
            
            return deferred.promise;
            
        },
        
        getLatestServices: function(){
            
            var deferred = $q.defer();
            
            var scope = this;
            
            $http.get('/getlatestservices.json')
                        .success(function(data, status, headers, config){
                            deferred.resolve(data);
                        })
                        .error(function(){
                            deferred.reject();
                        });
            
            return deferred.promise;
            
        },
        
        saveService: function(service){
            
            var deferred = $q.defer();
            
            var scope = this;
            
            $http.post('/services.json', service)
                    .success(function(data, status, headers, config){
                        deferred.resolve(data);
                    })
                    .error(function(){
                        deferred.reject();
                    })
            return deferred.promise
            
        },
        
        deleteService: function(serviceId){
            
            var deferred = $q.defer();
            
            var scope = this;
            
            console.log('deleting'+serviceId);
            
            $http.delete('/services/'+serviceId+'.json')
                    .success(function(data, status, headers, config){
                        console.log(status)
                        deferred.resolve(status);
                    })
                    .error(function(){
                        deferred.reject();
                    })
            return deferred.promise
            
        }
        
    }
    
    return serviceFactory;
        
}]);
