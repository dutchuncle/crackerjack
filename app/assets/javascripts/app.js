'use strict';

// Declare app level module which depends on views, and components
angular.module('crackerjackApp', [
                          'ui.router',
                          'templates',
                          'crackerjack.Menu',
                          'crackerjack.Dashboard',
                          'crackerjack.Subscriptions',
                          'crackerjack.Profile',
                          'crackerjack.Service',
                          'crackerjack.Managers'
                          ]).
    config(function($stateProvider, $urlRouterProvider) {
              $urlRouterProvider.otherwise("/dashboard");
    });
