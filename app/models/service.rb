class Service
  include Mongoid::Document
  include Mongoid::Timestamps
  field :name, type: String
  field :description, type: String
  field :week, type: Integer
  field :month, type: Integer
  field :year, type: Integer
  belongs_to :user
end
