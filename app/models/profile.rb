class Profile
  include Mongoid::Document
  include Mongoid::Timestamps
  field :username, type: String
  field :firstname, type: String, :default => ""
  field :lastname, type: String, :default => ""
  field :description, type: String
  belongs_to :user
end
