json.array!(@services) do |service|
  json.extract! service, :id, :name, :description, :week, :month, :year, :created_at
  json.url service_url(service, format: :json)
end
