/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('crackerjack.Profile', ['ui.router','templates'])
    .config(function($stateProvider, $urlRouterProvider) {
              $stateProvider
                  .state('profile', {
                    url: "/profile",
                    templateUrl: 'Profile/_profile.html',
                    controller: 'ProfileCtrl',
                    data:{}
                  })
                  .state('editprofile', {
                    url: "/profile-edit",
                    templateUrl: 'Profile/_edit-profile.html',
                    controller: 'ProfileCtrl',
                    data:{}
                  });
    })
    .controller('ProfileCtrl', ['$scope', 'profileFactory', function($scope, profileFactory) {
            
            profileFactory.getCurrentProfile().then(function(data){
                
                console.log(data);
                $scope.profile = data;
                
            });
            
            $scope.saveStatus = null;
            
            $scope.saveProfile = function(){
                
                console.log($scope.profile);
                
                profileFactory.saveProfile($scope.profile).then(function(data){
                    console.log(data);
                    $scope.saveStatus = "profile saved";
                });
                
            }
            
    }]);


